package com.example.bakery.kotlindemo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import java.math.BigDecimal

@Suppress("SENSELESS_COMPARISON")
class MainActivity : AppCompatActivity() {
    private var num1: Int = 0
    private var num2: Int = 0
    private var sum: Int = 0
    private var display: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnNine.setOnClickListener {
            setNumber(9)
        }

        btnEight.setOnClickListener {
            setNumber(8)
        }

        btnSeven.setOnClickListener {
            setNumber(7)
        }

        btnSix.setOnClickListener {
            setNumber(6)
        }

        btnFive.setOnClickListener {
            setNumber(5)
        }

        btnFour.setOnClickListener {
            setNumber(4)
        }

        btnThree.setOnClickListener {
            setNumber(3)
        }

        btnTwo.setOnClickListener {
            setNumber(2)
        }

        btnOne.setOnClickListener {
            setNumber(1)
        }

        btnZero.setOnClickListener {
            setNumber(0)
        }

        btnPlus.setOnClickListener {
            num1 = tvDisplay.text.toString().replace(",", "").toInt()
            tvDisplay.text = "0"

        }

        btnSum.setOnClickListener {
            num2 = tvDisplay.text.toString().replace(",", "").toInt()
            sum = num1 + num2
            tvDisplay.text = Content.CURRENCY_INTEGER.format(sum)

        }

        btnClear.setOnClickListener {
            tvDisplay.text = ""
        }
    }

    private fun setNumber(value: Int) {
        display = tvDisplay.text.toString().replace(",", "") + value
        val big = BigDecimal(display)
        tvDisplay.text = Content.CURRENCY_INTEGER.format(big)
    }

}
