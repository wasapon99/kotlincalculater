package com.example.bakery.kotlindemo

import java.text.DecimalFormat

/**
 * Created by Bakery on 3/29/2018.
 */

object Content {
    val CURRENCY_INTEGER = DecimalFormat("#,###")
    val CURRENCY_REAL = DecimalFormat("#,###.##")
}
